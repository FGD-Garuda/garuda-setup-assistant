#ifndef SETUPASSISTANT_H
#define SETUPASSISTANT_H

#include <QMainWindow>

#include <QAbstractButton>
#include <QtNetwork/QNetworkAccessManager>

QT_BEGIN_NAMESPACE
namespace Ui {
class SetupAssistant;
}
QT_END_NAMESPACE

class SetupAssistant : public QMainWindow {
    Q_OBJECT

public:
    enum class State {
        QUIT,
        WELCOME,
        INTERNET,
        UPDATE,
        UPDATE_RETRY,
        NVIDIA_CHECK,
        NVIDIA,
        NVIDIA_APPLY,
        SELECT,
        APPLY,
        APPLY_RETRY,
        SUCCESS
    };

    SetupAssistant(QWidget* parent = nullptr, QString state = "WELCOME");
    ~SetupAssistant();

private slots:
    void on_textWidget_buttonBox_clicked(QAbstractButton* button);
    void on_selectWidget_buttonBox_clicked(QAbstractButton* button);

private:
    Ui::SetupAssistant* ui;
    QDateTime executable_modify_date;

    State currentState;

    void doInternetUpRequest();
    void doUpdate();
    void doApply();
    void doNvidiaCheck();
    void doNvidiaApply();
    void populateSelectWidget();
    void populateSelectWidget(QString filename, QString label);
    void updateState(State state);
    void updateState(QString state);
    void relaunchSelf(QString param);
};
#endif // SETUPASSISTANT_H
